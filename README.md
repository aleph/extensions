# Aleph: Extensions

Extensions for Aleph.

## Plugins

Plugins can extend Aleph at run time.

To build a plugin, execute `go build -buildmode=plugin .`. To load a plugin,
evaluate `LoadPlugin["/path/to/plugin.so"]`.

Plugins must expose a symbol, `func Install(kernel.Kernel) error`, which will be
called when the plugin is loaded.

## Modules

Modules can extend Aleph at build time.

Including a module in an Aleph kernel requires modification of the kernel
executable's code. The simplest method is to install the module via a package's
`init()` function. Thus, a simple `import _ "example.com/your/aleph/ext` will
include the module.

The mainline Aleph kernel executable supports some modules this way, behind
build tags.

## Hello Extension

The `hello` extension prints "Hello!" to the kernel's stdout and defines a
function, `Hello[_]`, that prints its argument to the kernel's stdout.

## CAS Extension

***NOT PRODUCTION READY***

The `cas` extension is an attempt to create a computer algebra system. It is not
ready to be used in any capacity.