package cas

import (
	"gitlab.com/aleph-project/form"
	"gitlab.com/aleph-project/kernel"
	syms "gitlab.com/aleph-project/kernel/symbols"
)

func Differentiate(expression form.Form, symbols []form.SymbolForm, kernel kernel.Kernel) (form.Form, error) {
	return syms.NewStringError("Not implemented"), nil
}
