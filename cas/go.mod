module gitlab.com/aleph-project/extensions/cas

require (
	gitlab.com/aleph-project/form v0.2.12
	gitlab.com/aleph-project/kernel v0.0.0-20181207052416-ed35d039edc3
)

// https://github.com/golang/go/issues/27751
replace gitlab.com/aleph-project/kernel => ../../kernel
