package impl

import (
	"gitlab.com/aleph-project/extensions/cas"
	"gitlab.com/aleph-project/form"
	"gitlab.com/aleph-project/kernel"
	"gitlab.com/aleph-project/kernel/debug"
	"gitlab.com/aleph-project/kernel/impl"
	"gitlab.com/aleph-project/kernel/state"
	"gitlab.com/aleph-project/kernel/symbols"
)

var DifferentiateState = state.NewSymbolData(DifferentiateForm)

// NewSymbolValue(predicate, definition, returns form.Form, operation kernel.Operation, makeScope func() kernel.Scope)

func initDifferentiateState() {
	var attr = DifferentiateState.Attributes()
	attr.Set(symbols.ProtectedForm)
	attr.Set(symbols.LockedForm)
	attr.Set(symbols.HoldFirstForm)

	DifferentiateState.SetInnerValues(false, state.NewSymbolValue(
		symbols.NewPredicateWith(DifferentiateHead, false, symbols.NewEmptyBlank(), symbols.NewBlank("Symbol")),
		nil, nil,
		func(input form.Form, scope kernel.Scope, kernel kernel.Kernel) (result form.Form, altered bool, err error) {
			expression, ok := input.GetElement(0)
			if !ok {
				debug.Logger.Logf("failed to get element 0 of %v", input)
				return form.Null, true, impl.ErrOperationInputViolatesExpectations
			}

			symbol, ok := input.GetElement(1)
			if !ok {
				debug.Logger.Logf("failed to get element 1 of %v", input)
				return form.Null, true, impl.ErrOperationInputViolatesExpectations
			}

			return symbols.NewForm(DifferentiateHead, expression, form.NewList(symbol)), true, nil
		},
		nil,
	))

	DifferentiateState.SetInnerValues(false, state.NewSymbolValue(
		symbols.NewPredicateWith(DifferentiateHead, false, symbols.NewEmptyBlank(), form.NewList(symbols.NewBlank("Symbol"))),
		nil, nil, differentiateOperation, nil,
	))
}

func differentiateOperation(input form.Form, scope kernel.Scope, kernel kernel.Kernel) (result form.Form, altered bool, err error) {
	expression, ok := input.GetElement(0)
	if !ok {
		debug.Logger.Logf("failed to get element 0 of %v", input)
		return form.Null, true, impl.ErrOperationInputViolatesExpectations
	}

	symbols1, ok := input.GetElement(1)
	if !ok {
		debug.Logger.Logf("failed to get element 1 of %v", input)
		return form.Null, true, impl.ErrOperationInputViolatesExpectations
	}

	symbols2, ok := symbols1.(form.TensorForm)
	if !ok || symbols2.Dimensionality() != 1 {
		debug.Logger.Logf("expected tensor for element 1 of %v", input)
		return form.Null, true, impl.ErrOperationInputViolatesExpectations
	}

	symbols3 := symbols2.GetElements()
	symbols := make([]form.SymbolForm, len(symbols3))
	for i, el := range symbols3 {
		symbols[i], ok = el.(form.SymbolForm)
		if !ok {
			debug.Logger.Logf("expected tensor of symbols for element 1 of %v", input)
			return form.Null, true, impl.ErrOperationInputViolatesExpectations
		}
	}

	result, err = cas.Differentiate(expression, symbols, kernel)
	return result, true, err
}
