package impl

import (
	"gitlab.com/aleph-project/form"
	"gitlab.com/aleph-project/kernel/state"
)

func initGeneratedSymbols(data map[form.SymbolForm]*state.SymbolData) {
	data[DifferentiateForm] = DifferentiateState
}
