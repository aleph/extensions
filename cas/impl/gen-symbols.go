package impl

import "gitlab.com/aleph-project/form"

var (
	// Differentiate differentiates a symbolic algebra expression
	Differentiate = "Differentiate"

	// DifferentiateForm is the symbol form of Differentiate
	DifferentiateForm = form.SymbolForm(Differentiate)

	// DifferentiateHead is the head for Differentiate forms
	DifferentiateHead = form.NewUnknownSymbolicHead(Differentiate)
)
