package impl

import (
	"errors"
	"log"

	"gitlab.com/aleph-project/kernel"
)

//go:generate generate-symbols impl ../symbols.yml gen-symbols.go
//go:generate generate-state impl ../symbols.yml gen-states.go

func init() {
	initDifferentiateState()
}

// Install installs the CAS plugin into the kernel
func Install(k kernel.Kernel) error {
	var s = k.State()

	if !s.InstallSymbol(DifferentiateForm, DifferentiateState) {
		return errors.New("failed to install Differentiate")
	} else {
		log.Println("installed Differentiate")
	}

	return nil
}
