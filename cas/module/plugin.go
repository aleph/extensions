package mod

import (
	"log"

	cas "gitlab.com/aleph-project/extensions/cas/impl"
	"gitlab.com/aleph-project/kernel/impl"
)

func init() {
	log.Print("installing CAS module")
	k := impl.NewGlobalKernel()
	err := cas.Install(k)
	if err != nil {
		panic(err)
	}
}
