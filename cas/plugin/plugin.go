package main

import (
	"fmt"
	"os"

	cas "gitlab.com/aleph-project/extensions/cas/impl"
	"gitlab.com/aleph-project/kernel"
)

func main() {
	fmt.Println("Must be loaded as a plugin")
	os.Exit(1)
}

// Install installs the cas plugin into the kernel
func Install(k kernel.Kernel) error {
	return cas.Install(k)
}
