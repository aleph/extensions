package hello

import (
	"fmt"

	"gitlab.com/aleph-project/form"
	"gitlab.com/aleph-project/kernel"
	"gitlab.com/aleph-project/kernel/state"
	"gitlab.com/aleph-project/kernel/symbols"
)

// Install installs the hello plugin into the kernel
func Install(k kernel.Kernel) error {
	// creates a function, Hello[_], that prints the argument and returns ()
	k.State().GetOrCreateSymbol(form.SymbolForm("Hello")).SetInnerValues(false, state.NewSymbolValue(
		symbols.NewPredicateWith(form.NewUnknownSymbolicHead("Hello"), false, symbols.NewEmptyBlank()),
		nil, nil,
		func(input form.Form, _ kernel.Scope, _ kernel.Kernel) (form.Form, bool, error) {
			fmt.Println(input)
			return form.Null, true, nil
		},
		nil,
	))

	return nil
}
