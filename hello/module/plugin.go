package mod

import (
	"log"

	"gitlab.com/aleph-project/extensions/hello"
	"gitlab.com/aleph-project/kernel/impl"
)

func init() {
	log.Print("installing Hello module")
	k := impl.NewGlobalKernel()
	err := hello.Install(k)
	if err != nil {
		panic(err)
	}
}
