package main

import (
	"fmt"
	"os"

	"gitlab.com/aleph-project/extensions/hello"
	"gitlab.com/aleph-project/kernel"
)

func main() {
	fmt.Println("Must be loaded as a plugin")
	os.Exit(1)
}

// Install installs the hello plugin into the kernel
func Install(k kernel.Kernel) error {
	fmt.Println("Hello!")
	return hello.Install(k)
}
